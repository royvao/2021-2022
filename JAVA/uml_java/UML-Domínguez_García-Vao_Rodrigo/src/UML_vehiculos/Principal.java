package UML_vehiculos;

import java.util.ArrayList;

public class Principal {
	
	public static ArrayList<Vehiculo> vehiculos;
	public static ArrayList<Avion> aviones;
	public static ArrayList<Coche> coches;

	public static void main(String[] args) {
		
		Menu.menu(vehiculos, aviones, coches);
		System.out.println("");
		CrearVehiculo.verVehiculos();
	}
}
