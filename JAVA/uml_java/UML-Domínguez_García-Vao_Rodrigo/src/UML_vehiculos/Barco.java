package UML_vehiculos;

public class Barco extends Vehiculo{

	private int eslora, calado;
	private Tipo tipo;
	public Barco(int eslora, int calado, Tipo tipo, String marca, String color, 
			String numBastidor, int kilometros, int añoFabricacion) {
		super(marca, color, numBastidor, kilometros, añoFabricacion);
		this.eslora = eslora;
		this.calado = calado;
		this.tipo = tipo;
	}
	
	public int getEslora() {
		return eslora;
	}

	public void setEslora(int eslora) {
		this.eslora = eslora;
	}

	public int getCalado() {
		return calado;
	}

	public void setCalado(int calado) {
		this.calado = calado;
	}

	
	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Barco [eslora=" + eslora + ", calado=" + calado + ", Tipo=" + tipo + ", marca=" + marca + ", color="
				+ color + ", numBastidor=" + numBastidor + ", kilometros=" + kilometros + ", añoFabricacion="
				+ añoFabricacion + "]";
	}


	
	
}

	
