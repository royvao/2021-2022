package UML_vehiculos;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class CrearVehiculo {

	static Scanner leer = new Scanner(System.in);

	public static ArrayList<Vehiculo> vehiculos = new ArrayList<>();
	public static ArrayList<Avion> aviones = new ArrayList<>();
	public static ArrayList<Coche> coches = new ArrayList<>();

	static String marca, color, numBastidor, numBastidorSinTruncar;
	static int kilometros, añoFabricacion, eslora, calado, parar, optTipo;
	static boolean electrico, antiguo, combate, optCrear;
	static byte motores;
	static double velocidadMax;
	static Tipo tipo;

	public static void infoVehiculo() {
		try {

			System.out.print("Introduce la marca del vehículo: ");
			marca = leer.nextLine();

			System.out.print("Introduce color del vehículo: ");
			color = leer.nextLine();
		} catch(InputMismatchException e) {
			System.err.print("Sólo se permiten carácteres.\n");
			Menu.menu(vehiculos, aviones, coches);
		}catch (Exception e) {
			System.err.print("Ha ocurrido un error inesperado.\n");
			Menu.menu(vehiculos, aviones, coches);
		}

	
		System.out.print("Introduce el número de bastidor del vehículo: ");
		numBastidor = leer.nextLine();

		try {
			System.out.print("Introduce el kilometraje del vehículo: ");
			kilometros = leer.nextInt();

			if (kilometros < 0) {
				System.err.println("\nEl valor introducido no es correcto.");	
				leer.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}

			System.out.print("Introduce el año de fabricación del vehículo: ");
			añoFabricacion = leer.nextInt();
			
			if(añoFabricacion<1900 && añoFabricacion >2022) {
				System.err.println("El año introducido debe estar entre 1900 y 2022.");
				Menu.menu(vehiculos, aviones, coches);
			}
		} catch (Exception e) {
			System.err.println("Sólo se permiten valores numéricos.");
			Menu.menu(vehiculos, aviones, coches);
		}


	}

	public static void crearBarco() {
		infoVehiculo();
		try {
			
			System.out.print("Introduce la eslora del barco: ");
			eslora = leer.nextInt();
			if(eslora<0) {
				System.err.println("\nEl valor introducido no es correcto.");	
				leer.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}
			
			System.out.print("Introduce el calado del barco: ");
			calado = leer.nextInt();
			if(calado<0) {
				System.err.println("\nEl valor introducido no es correcto.");	
				leer.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}
			
			System.out.print("Introduce el tipo de barco: \n 1- Vela. \n 2- Pesca. \n 3- Pasajeros.");
			optTipo = leer.nextInt();
			if(optTipo <=3 && optTipo>0) {
				switch(optTipo) {
				case 1:
					tipo = Tipo.vela;
					break;
				case 2:
					tipo = Tipo.pesca;
					break;
				case 3:
					tipo = Tipo.pasajeros;
					break;
				}
			}else {
				System.err.println("El valor debe ser un número entre 1 y 3.");
				Menu.menu(vehiculos, aviones, coches);
			}
			
		} catch (InputMismatchException e) {
			System.err.println("Sólo se permiten valores numéricos");
			Menu.menu(vehiculos, aviones, coches);
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error inesperado.");
			Menu.menu(vehiculos, aviones, coches);
		}

		Barco b1 = new Barco(eslora,calado, tipo, marca, color, numBastidor, kilometros, añoFabricacion);

		vehiculos.add(b1);
	}

	public static void crearAvion() {
		infoVehiculo();
		try {
			System.out.print("Introduce el número de motores del avión: ");
			motores = (byte) leer.nextInt();
			if(motores<0) {
				System.err.println("\nEl valor introducido no es correcto.");	
				leer.nextLine();
				infoVehiculo();
			}
			
			System.out.print("Introduce la velocidad máxima del avión: ");
			velocidadMax= leer.nextInt();
			if(velocidadMax<0) {
				System.err.println("\nEl valor introducido no es correcto.");	
				leer.nextLine();
				infoVehiculo();
			}
			
			System.out.print("¿Es de combate? true/ false ");
			combate = leer.nextBoolean();
		}catch(InputMismatchException e) {
			System.err.println("El formato de datos de los campos debe ser el siguiente: ");
			System.err.println(" - Motores -> Número entre 0 y 127.\n - Velocidad máxima -> Un carácter numérico.\n - Combate -> True o false.");
			Menu.menu(vehiculos, aviones, coches);
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error inesperado.");
			Menu.menu(vehiculos, aviones, coches);
		}



		Avion a1 = new Avion(motores, velocidadMax, combate, marca, color, numBastidor, kilometros, añoFabricacion);
		Avion a2 = new Avion(motores, (velocidadMax+3000), combate, marca, color, numBastidor, kilometros, añoFabricacion);

		vehiculos.add(a1);
		vehiculos.add(a2);
		aviones.add(a1);
		aviones.add(a2);
	}

	public static void crearCoche() {
		infoVehiculo();
		try {
			System.out.print("¿Es eléctrico? true/false ");
			electrico = leer.nextBoolean();
			System.out.print("¿Es antiguo? true/false ");
			antiguo = leer.nextBoolean();
		}catch(InputMismatchException e) {
			System.err.println("El formato de este campo es true o false.");
			Menu.menu(vehiculos, aviones, coches);
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error inesperado.\n");
			Menu.menu(vehiculos, aviones, coches);
		}

		Coche c1 = new Coche(electrico, antiguo, marca, color, numBastidor, kilometros, añoFabricacion);
		Coche c2 = new Coche(electrico=false, antiguo, marca, color, numBastidor, kilometros, añoFabricacion);

		vehiculos.add(c1);
		vehiculos.add(c2);
		coches.add(c1);
		coches.add(c2);


	}
	public static void verVehiculos() {
		for(int i = 0 ; i<vehiculos.size(); i++) {
			System.out.println(vehiculos.get(i).toString());
		}
	}

	public static int crearMas() {

		System.out.print("¿Desear hacer algo más? true/false ");
		try {
			optCrear = leer.nextBoolean();
			leer.nextLine();
		}catch(InputMismatchException e){
			System.out.println("El valor que introduzca debe ser true o false.\n");
			Menu.menu(vehiculos, aviones, coches);
		}

		if(optCrear==true) {
			Menu.menu(vehiculos, aviones, coches);
		}else if (optCrear == false) {
			parar = 1;
		}

		return parar;
	}
	
	public static String numBastidorMaxChars() {
		int max_char=17;

		if(numBastidorSinTruncar==null) {
			return null;
		}
		return numBastidorSinTruncar.length() < max_char ? numBastidorSinTruncar : numBastidorSinTruncar.substring(0,max_char);
	}



}
