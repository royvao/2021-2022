package UML_vehiculos;

public class Coche extends Vehiculo{
	
	protected boolean electrico;
	private boolean antiguo;
	
	public Coche(boolean electrico, boolean antiguo, String marca, String color, String numBastidor, int kilometros, int añoFabricacion) {
		super(marca, color, numBastidor, kilometros, añoFabricacion);
		this.electrico= electrico;
		this.antiguo=antiguo;
	}

	public boolean isElectrico() {
		return electrico;
	}

	public void setElectrico(boolean electrico) {
		this.electrico = electrico;
	}

	public boolean isAntiguo() {
		return antiguo;
	}

	public void setAntiguo(boolean antiguo) {
		this.antiguo = antiguo;
	}

	@Override
	public String toString() {
		return "Coche [electrico=" + electrico + ", antiguo=" + antiguo + ", marca=" + marca + ", color=" + color
				+ ", numBastidor=" + numBastidor + ", kilometros=" + kilometros + ", añoFabricacion=" + añoFabricacion
				+ "]";
	}

	
	
	
}
