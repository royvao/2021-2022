package entorno;

import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;

import javax.swing.border.SoftBevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.text.Highlighter;
import javax.swing.border.BevelBorder;
import java.awt.Font;
import java.awt.Cursor;

@SuppressWarnings("unused")
public class GamePanel extends JPanel implements KeyListener {

	private static final long serialVersionUID = 1L;
	public static final int EASY = 1;
	public static final int HARD = 2;

	private MFrame frame;
	private KeyboardPanel keyboard;
	private StatsPanel stats;
	private JTextArea areaText, areaInput;
	private String[] textos;
	private JButton[] btArray;
	
	int fails = 0;
	int maxFails = 0;
	int pulsations = 0;

	public void panelsPropieties(JPanel panels) {
		panels.setBorder(new SoftBevelBorder(BevelBorder.RAISED, new Color(128, 0, 255), new Color(128, 0, 255),
				new Color(64, 0, 128), null));
		panels.setBackground(new Color(255, 255, 255));
	}

	public GamePanel(MFrame frame) {
		this.frame = frame;

		// PROPIEDADES DEL PANEL
		setVisible(true);
		setLayout(null);
		setBackground(new Color(192, 192, 192));

		// ESCALAR LA IMAGEN DE FONDO A PANTALLA COMPLETA
		Image bckgrnd = new ImageIcon("images\\fondo-leccion-facil.jpg").getImage();
		ImageIcon bckgrnd2 = new ImageIcon(
				bckgrnd.getScaledInstance(frame.getScrnSize().width, frame.getScrnSize().height, Image.SCALE_SMOOTH));

		// AREA SUPERIOR DEL TEXTO
		areaText = new JTextArea();
		areaText.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		areaText.setBounds(30, 20, 1300, 330);
		areaText.setBorder(new SoftBevelBorder(BevelBorder.RAISED, new Color(128, 0, 255), new Color(128, 0, 255), new Color(64, 0, 128), null));
		areaText.setFocusable(false);
		areaText.setEditable(false);
		areaText.setLineWrap(true);
		add(areaText);

		//AREA INFERIOR DEL TEXTO
		areaInput = new JTextArea();
		areaInput.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		areaInput.setBorder(new SoftBevelBorder(BevelBorder.RAISED, new Color(128, 0, 255), new Color(128, 0, 255), new Color(64, 0, 128), null));
		areaInput.setBounds(areaText.getLocation().x , areaText.getLocation().y+350, areaText.getWidth(), areaText.getHeight()/3);
		areaInput.setEditable(true);
		areaInput.setLineWrap(true);
		areaInput.setFocusable(true);
		areaInput.addKeyListener(this);
		add(areaInput);

		// DECLARAMOS EL PANEL PARA LAS ESTADISTICAS
		stats = new StatsPanel(this);
		add(stats);

		// DECLARAMOS EL PANEL PARA CREAR EL TEACLO
		keyboard = new KeyboardPanel(this);
		add(keyboard);

		// PROPIEDADES DEL BOTÓN "SALIR"
		JButton btnSave = new JButton("Volver al menú");
		btnSave.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// SI SE PULSA SE OCULTA LA PANTALLA DE JUEGO Y SE MUESTRA EL MENU
				frame.dispose();
				frame.setUndecorated(false);
				
				frame.setSize(550, 440);
				frame.getGame().setVisible(false);
				frame.getMenu().setVisible(true);
				frame.setLocationRelativeTo(null);
				
				frame.setVisible(true);

			}
		});
		btnSave.setSize(300, 50);
		btnSave.setFocusable(false);
		btnSave.setLocation(stats.getLocation().x, stats.getLocation().y + stats.getHeight() + 10);
		add(btnSave);

		// ESTABLECER IMAGEN DE EN EL FONDO CON UN LABEL
		JLabel fondo = new JLabel("");
		fondo.setBounds(0, 0, frame.getScrnSize().width, frame.getScrnSize().height);
		fondo.setIcon(bckgrnd2);
		fondo.setFocusable(false);
		add(fondo);

		btArray = keyboard.getBtArray();

	}

	public void loadTexts(int difficulty) {
		switch (difficulty) {
		case GamePanel.EASY:
			areaText.setText(textos[0]);
			break;
		case GamePanel.HARD:
			areaText.setText(textos[1]);
			break;
		}

	}
	
	// FUNCIÓN PARA CONTAR FALLOS
	
	public void failCount() {
		setFails(getFails()+1);
		stats.getFailsCounter().setText(String.valueOf(getFails()));
		
		if(getFails() == maxFails) {
			stats.getCuentaAtras().stop();
			areaInput.setEnabled(false);
			JOptionPane.showMessageDialog(null, "Muchos errores...Necesitas mejorar. Estas son tus estadísticas:"
					+ "\nPPM: " + pulsations
					+ "\nTiempo: " + (stats.getMinTime().getText()), "Se acabó.",
					JOptionPane.INFORMATION_MESSAGE);	
			frame.dispose();
			frame.setUndecorated(false);
			
			frame.setSize(550, 440);
			frame.getGame().setVisible(false);
			frame.getMenu().setVisible(true);
			frame.setLocationRelativeTo(null);
			
			frame.setVisible(true);
		}
	}
	

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		String atlc = areaText.getText().toLowerCase();

		Highlighter highLight = areaText.getHighlighter();
		DefaultHighlightPainter green = new DefaultHighlighter.DefaultHighlightPainter(Color.green);
		DefaultHighlightPainter red = new DefaultHighlighter.DefaultHighlightPainter(Color.red);

		try {

			if (e.getKeyChar() == atlc.charAt(areaInput.getText().length())) {
				stats.getCuentaAtras().start();
				for (int i = 0; i < btArray.length; i++) {
					if (btArray[i].getText().toLowerCase().charAt(0) == e.getKeyChar()) {
						try {
							highLight.addHighlight(areaInput.getText().length(), areaInput.getText().length() + 1,
									green);
						} catch (BadLocationException e1) {
							e1.printStackTrace();
						}
						btArray[i].setBackground(Color.green);
						pulsations++;
					}
				}
			}else {
				failCount();
				for (int i = 0; i < btArray.length; i++) {
					if (btArray[i].getText().toLowerCase().charAt(0) == e.getKeyChar()) {
						try {
							highLight.addHighlight(areaInput.getText().length(), areaInput.getText().length() + 1, red);
						} catch (BadLocationException e1) {
							e1.printStackTrace();
						}
						btArray[i].setBackground(Color.red);
						pulsations++;
					}
				}
			 }
			stats.getKeyPress().setText(String.valueOf(pulsations));

			}catch(StringIndexOutOfBoundsException e1) {
					stats.getCuentaAtras().stop();
					areaInput.setEnabled(false);
					JOptionPane.showMessageDialog(null, "Enhorabuena, estas son tus estadísticas:"
							+ "\nTiempo: " + ((stats.getXT()) + (stats.getMinTime().getText()))
							+ "\nPPM: " + pulsations	
							+ "\nFallos: " + fails, "Se acabó.",JOptionPane.INFORMATION_MESSAGE);	
					frame.dispose();
					frame.setUndecorated(false);
					
					frame.setSize(550, 440);
					frame.getGame().setVisible(false);
					frame.getMenu().setVisible(true);
					frame.setLocationRelativeTo(null);
					
					frame.setVisible(true);

				}
		
	  
	}

	@Override
	public void keyReleased(KeyEvent e) {
		for (int i = 0; i < btArray.length; i++) {
			if (btArray[i].getText().toLowerCase().charAt(0) == e.getKeyChar()) {
				btArray[i].setBackground(Color.white);
			}
		}
	}
	
	// GETTERS Y SETTERS
	
		public void setTextos(String[] textos) {
			this.textos = textos;
		}
		

		public StatsPanel getStats() {
			return stats;
		}

		public void setStats(StatsPanel stats) {
			this.stats = stats;
		}

		
		public JButton[] getBtArray() {
			return btArray;
		}

		public void setBtArray(JButton[] btArray) {
			this.btArray = btArray;
		}

		public JTextArea getAreaInput() {
			return areaInput;
		}

		public void setAreaInput(JTextArea areaInput) {
			this.areaInput = areaInput;
		}

		public JTextArea getAreaText() {
			return areaText;
		}

		public void setAreaText(JTextArea areaText) {
			this.areaText = areaText;
		}

		public int getFails() {
			return fails;
		}

		public void setFails(int fails) {
			this.fails = fails;
		}

		public int getMaxFails() {
			return maxFails;
		}

		public void setMaxFails(int maxFails) {
			this.maxFails = maxFails;
		}
		
		public int getPulsations() {
			return pulsations;
		}

		public void setPulsations(int pulsations) {
			this.pulsations = pulsations;
		}

		public MFrame getFrame() {
			return frame;
		}

		public void setFrame(MFrame frame) {
			this.frame = frame;
		}
		
		
		// FIN GET Y SET
}
