package clientes_compras;
import java.util.*;

public class Compra {
	
	private String clave, texto, fecha;
	
	public Compra(String c, String t, String f) {
		clave = c;
		texto = t;
		fecha = f;
	}
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	
	
}
