package progress_bar;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

public class Progress {

	private JFrame frame;
	private JButton boton_start;
	private JLabel texto_final;
	private JProgressBar progressBar;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Progress window = new Progress();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Progress() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setUndecorated(true);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		texto_final = new JLabel("FINALLY FRIDAY!!!!");
		texto_final.setHorizontalAlignment(SwingConstants.CENTER);
		texto_final.setFont(new Font("Tahoma", Font.BOLD, 30));
		texto_final.setVisible(false);
		texto_final.setBackground(new Color(0, 255, 255));
		texto_final.setBounds(0, 0, 450, 60);
		frame.getContentPane().add(texto_final);

		progressBar = new JProgressBar();
		progressBar.setForeground(new Color(128, 255, 0));
		progressBar.setBackground(new Color(255, 0, 0));
		progressBar.setBounds(10, 175, 430, 28);
		frame.getContentPane().add(progressBar);

		lblNewLabel = new JLabel("✔");
		lblNewLabel.setVisible(false);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBackground(new Color(128, 255, 0));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setBounds(179, 90, 89, 60);
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Loading...");
		lblNewLabel_1.setVisible(false);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblNewLabel_1.setBounds(114, 90, 205, 60);
		frame.getContentPane().add(lblNewLabel_1);
		
		boton_start = new JButton("START");

		boton_start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Timer timer = new Timer();
				timer.scheduleAtFixedRate(new TimerTask() {
					int i = 10;

					@Override
					public void run() {
						progressBar.setValue(progressBar.getValue() + 10);
						i--;
						if(progressBar.getValue()==50) {
							boton_start.setVisible(false);
							lblNewLabel_1.setVisible(true);

						}
						if (i < 0) {
							timer.cancel();
							texto_final.setVisible(true);
							lblNewLabel.setVisible(true);
							lblNewLabel_1.setVisible(false);


						}
					}

				}, 0, 1000);
			}
		});

		boton_start.setBounds(179, 90, 89, 60);
		frame.getContentPane().add(boton_start);
		
		
		
	

	}
}
