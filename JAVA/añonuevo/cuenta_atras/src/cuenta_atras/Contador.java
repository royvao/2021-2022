package cuenta_atras;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;

public class Contador {

	private JFrame frame;
	private JButton boton_start;
	private JLabel texto_final;
	private JLabel number;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Contador window = new Contador();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Contador() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		number = new JLabel();
		number.setVisible(true);
		number.setFont(new Font("Tahoma", Font.BOLD, 30));
		number.setBounds(206, 90, 69, 60);
		frame.getContentPane().add(number);

		texto_final = new JLabel("FELIZ AÑO NUEVO!!!!");
		texto_final.setVisible(false);
		texto_final.setBackground(new Color(0, 128, 0));
		texto_final.setBounds(181, 11, 158, 28);
		frame.getContentPane().add(texto_final);

		boton_start = new JButton("START");

		boton_start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Timer timer = new Timer();
				timer.scheduleAtFixedRate(new TimerTask() {
					int i = 10;

					@Override
					public void run() {
						number.setText("" + i);
						i--;

						if (i < 0) {
							timer.cancel();
							texto_final.setVisible(true);
						}
					}

				}, 0, 1000);
			}
		});

		boton_start.setBounds(35, 90, 89, 60);
		frame.getContentPane().add(boton_start);

	}
}
