package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class delete {

	public static void main(String[] args) {
		String jdbc = "jdbc:mysql://";
		String host = "localhost";
		String port = "3306";
		String baseDatos = "ROY_BBDD";
		String urlConnection = jdbc + host + ":" + port + "/" + baseDatos;
		String usr = "root";
		String pwd = "";

		Connection miConexion = null;
		PreparedStatement sentencia = null;
		
		try {
			miConexion = DriverManager.getConnection(urlConnection, usr, pwd);
			System.out.println("¡USUARIO ELIMINADO!\n");

			@SuppressWarnings("unused")
			Statement miStatement = miConexion.createStatement();
			
			String sentenciaSQL = "DELETE FROM DATOS WHERE (id_alumno, nombre_alumno, apellido1_alumno, apellido2_alumno, DNI, email_alumno, telefono_alumno) = (?, ? ,?, ?, ?, ?, ?)";
			sentencia = miConexion.prepareStatement(sentenciaSQL);

			sentencia.setString(1, "5");
			sentencia.setString(2, "Manuel");
			sentencia.setString(3, "Garcia");
			sentencia.setString(4, "Gonalo");
			sentencia.setString(5, "5124156G");
			sentencia.setString(6, "manu@mail.es");
			sentencia.setString(7, "5676533425");
			sentencia.executeUpdate();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

}
