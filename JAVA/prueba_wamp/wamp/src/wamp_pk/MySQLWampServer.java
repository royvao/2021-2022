package wamp_pk;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLWampServer {
	public static void main(String[] args) {
		String jdbc = "jdbc:mysql://";
		String host = "localhost";
		String port = "3306";
		String baseDatos = "ROY_BBDD";
		String urlConnection = jdbc + host + ":" + port + "/" + baseDatos;
		String usr = "root";
		String pwd = "";
		
		Connection miConexion = null;
		
				try {
			miConexion = DriverManager.getConnection(urlConnection, usr, pwd);
			System.out.println("¡Conexión ABIERTA!\n");
			
			Statement miStatement = miConexion.createStatement();
			
			String sentencia = ("SELECT * FROM DATOS ORDER BY ID_ALUMNO");
			ResultSet miResultset = miStatement.executeQuery(sentencia);
			
			while(miResultset.next()) {
				System.out.println(
						  " | "	+ miResultset.getString("id_alumno")
						+ " | "	+ miResultset.getString("apellido1_alumno")
						+ " "	+ miResultset.getString("apellido2_alumno")
						+ " | "	+ miResultset.getString("nombre_alumno")
						+ " | "	+ miResultset.getString("DNI")
						+ " | "	+ miResultset.getString("email_alumno")
						+ " | "	+ miResultset.getString("telefono_alumno")
						+ " | "
						);
			}
			miStatement.close();
			System.out.println();
			System.out.println("¡Statement CERRADA!");
		}catch (SQLException sqle) {
			muestraErrorSQL(sqle);
			
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			if (miConexion != null) {
				try {
					miConexion.close();
					System.out.println("¡Base de Datos CERRADA!");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	public static void muestraErrorSQL(SQLException e) {
		System.err.println("SQL ERROR mensaje: " + e.getMessage());
		System.err.println("SQL estado: " + e.getSQLState());
		System.err.println("SQL código específico: " + e.getErrorCode());
	}
}
