﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Buscaminas_WPF
{
    public class Celda : INotifyPropertyChanged
    {
        #region Attributes
        int row;
        int column;
        string text;
        bool mina = false;
        Visibility showBomb = Visibility.Hidden;
        Visibility showFlag = Visibility.Hidden;
        Visibility showQuestion = Visibility.Hidden;
        #endregion

        #region Properties
        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                RaisePropertyChanged(nameof(Text));
            }
        }

        public int Row
        {
            get { return row; }
            set { row = value; }
        }

        public int Column
        {
            get { return column; }
            set { column = value; }
        }

        public bool Mina
        {
            get
            {
                return mina;
            }

            set
            {
                mina = value;
            }
        }

        public Visibility ShowBomb
        {
            get
            {
                return showBomb;
            }

            set
            {
                showBomb = value;
                RaisePropertyChanged(nameof(ShowBomb));
            }
        }

        public Visibility ShowFlag
        {
            get
            {
                return showFlag;
            }

            set
            {
                showFlag = value;
                RaisePropertyChanged(nameof(ShowFlag));
            }
        }

        public Visibility ShowQuestion
        {
            get
            {
                return showQuestion;
            }

            set
            {
                showQuestion = value;
                RaisePropertyChanged(nameof(ShowQuestion));
            }
        }
        #endregion


        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
