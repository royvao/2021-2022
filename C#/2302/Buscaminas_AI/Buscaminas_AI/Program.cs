﻿using System;

class Program
{
    static void Main(string[] args)
    {
        int[,] mineField = CreateMineField();
        PrintMineField(mineField);
        PlayGame(mineField);
    }

    static int[,] CreateMineField()
    {
        int[,] mineField = new int[5, 5];

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                mineField[i, j] = 0;
            }
        }

        Random rnd = new Random();
        int minesToPlace = 5;
        while (minesToPlace > 0)
        {
            int x = rnd.Next(5);
            int y = rnd.Next(5);
            if (mineField[x, y] == 0)
            {
                mineField[x, y] = 9;
                minesToPlace--;
            }
        }

        return mineField;
    }

    static void PrintMineField(int[,] mineField)
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                Console.Write(mineField[i, j] + " ");
            }
            Console.WriteLine();
        }
    }

    static void PlayGame(int[,] mineField)
    {
        int x = 0;
        int y = 0;
        while (true)
        {
            Console.WriteLine("Enter x coordinate (0-4): ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y coordinate (0-4): ");
            y = int.Parse(Console.ReadLine());

            if (mineField[x, y] == 9)
            {
                Console.WriteLine("Game over!");
                break;
            }
            else
            {
                Console.WriteLine("Safe!");
            }
        }
    }
}
