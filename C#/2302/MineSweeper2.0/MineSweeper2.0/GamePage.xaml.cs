﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Data.SqlClient;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;

#region quitAdverts
// Quitar adevertencias
#pragma warning disable CS8600
#pragma warning disable	CS0252
#pragma warning disable	CS8622
#pragma warning disable	CS8625
#pragma warning disable	CS8618
#endregion

namespace MineSweeper2._0
{
    /// <summary>
    /// Lógica de interacción para GamePage.xaml
    /// </summary>
    public partial class GamePage : Page
    {
        #region components
        int nMinas;
        int discoveredCells;
        int nRows = 10;
        int nCols = 10;
        string selectedDifficulty;
        private TimeSpan elapsed;
        private Image bombImage;
        DispatcherTimer timer;
        private readonly UserViewModel _userViewModel;
        string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Usuarios;user id = sa; password = mssqlserver";
        #endregion
        public GamePage(UserViewModel userViewModel)
        {
            #region allGameInfo
            InitializeComponent();
            _userViewModel = userViewModel;
            startButton.IsEnabled = false;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            Loaded += GamePage_Loaded;
            bombImage = new Image();
            bombImage.Source = new BitmapImage(new Uri("img/bomb.png", UriKind.Relative));

            #endregion
        }

        #region gamePage_Loaded
        private void GamePage_Loaded(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Width = 625;
            parentWindow.Height = 500;
        }
        #endregion

        #region timer_Tick
        private void Timer_Tick(object sender, EventArgs e)
        {
            // Incrementa el tiempo transcurrido en un segundo
            elapsed = elapsed.Add(TimeSpan.FromSeconds(1));

            // Verifica si el tiempo transcurrido es inferior a un minuto
            if (elapsed.TotalSeconds < 60)
            {
                // Muestra los segundos en el temporizador
                timeElapsedLabel.Content = "Tu tiempo: " + elapsed.Seconds.ToString("D2") + " segundos.";
            }
            else
            {
                // Muestra los minutos y segundos en el temporizador
                timeElapsedLabel.Content = "Tu tiempo: " + elapsed.ToString(@"m\:ss");
            }
        }
        #endregion

        #region difficultyComboBox_SelectionChanged
        private void difficultyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            startButton.IsEnabled = true;

            // Obtiene la selección del usuario
            if (difficultyComboBox.SelectedIndex == 0)
            {
                nMinas = 10;
                selectedDifficulty = "Fácil";
                SetGridSize(selectedDifficulty);
            }
            else if (difficultyComboBox.SelectedIndex == 1)
            {
                nMinas = 20;
                selectedDifficulty = "Intermedio";
                SetGridSize(selectedDifficulty);
            }
            else if (difficultyComboBox.SelectedIndex == 2)
            {
                nMinas = 30;
                selectedDifficulty = "Difícil";
                SetGridSize(selectedDifficulty);
            }
        }

        #endregion

        #region countMinesAroundButton
        // Cuenta las minas alrededor de un botón
        private int CountMinesAroundButton(int row, int col)
        {
            int count = 0;

            for (int i = row - 1; i <= row + 1; i++)
            {
                for (int j = col - 1; j <= col + 1; j++)
                {
                    if (i >= 0 && i < nRows && j >= 0 && j < nCols)
                    {
                        Button button = (Button)mainGrid.Children
                            .OfType<Button>()
                            .FirstOrDefault(b => Grid.GetRow(b) == i && Grid.GetColumn(b) == j);
                        if (button != null && button.Content == "")
                        {
                            count++;
                        }
                    }
                }
            }

            return count;
        }
        #endregion

        #region discoverEmptyCells
        private void DiscoverEmptyCells(int row, int col)
        {
            for (int i = row - 1; i <= row + 1; i++)
            {
                for (int j = col - 1; j <= col + 1; j++)
                {
                    if (i >= 0 && i < nRows && j >= 0 && j < nCols)
                    {
                        Button button = (Button)mainGrid.Children
                            .OfType<Button>()
                            .FirstOrDefault(b => Grid.GetRow(b) == i && Grid.GetColumn(b) == j);
                        if (button != null && button.Content != "" && button.IsEnabled)
                        {
                            int count = CountMinesAroundButton(i, j);
                            button.Content = count.ToString();                            
                            UpdateDiscoveredCells(_userViewModel);
                            if (count == 0)
                            {
                                button.IsEnabled = false;
                                DiscoverEmptyCells(i, j);
                            }
                            else
                            {
                                button.IsEnabled = false;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region updateDiscoveredCells
        public void UpdateDiscoveredCells(UserViewModel userViewModel)
        {
            var menuPage = new MenuPage(_userViewModel);
            discoveredCells++;
            if (discoveredCells == (nRows * nCols - nMinas))
            {

                timer.Stop();
                foreach (Button btn in mainGrid.Children.OfType<Button>())
                {
                    if (btn.Content == "")
                    {
                        btn.Content = "X";
                        btn.IsEnabled = false;
                        btn.Foreground = Brushes.Red;
                    }
                    else
                    {
                        int row = Grid.GetRow(btn);
                        int col = Grid.GetColumn(btn);
                        int count = CountMinesAroundButton(row, col);
                        btn.Content = count.ToString();
                    }
                    btn.IsEnabled = false;
                    startButton.IsEnabled = false;
                    difficultyComboBox.IsEnabled = true;
                }
                if (elapsed.TotalSeconds < 60)
                {
                    // Muestra los segundos en el temporizador

                    MessageBox.Show("¡Enhorabuena " + _userViewModel.userName + ", has ganado la partida!\n" + "Has tardado " + elapsed.Seconds.ToString("D2") + " segundos.");
                    NavigationService.Navigate(menuPage);
                }
                else
                {
                    // Muestra los minutos y segundos en el temporizador
                    MessageBox.Show("¡Enhorabuena " + _userViewModel.userName + ", has ganado la partida!\n" + "Has tardado " + elapsed.ToString(@"m\:ss") + " minutos.");
                    NavigationService.Navigate(menuPage);
                }

                // Guardar las estadísticas en la base de datos
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                string sql = "INSERT INTO estadisticas (nombre, tiempo, dificultad, minas, fecha) VALUES (@jugador, @tiempo, @dificultad, @minas, @fecha)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@jugador", _userViewModel.userName);
                command.Parameters.AddWithValue("@tiempo", elapsed.TotalSeconds);
                command.Parameters.AddWithValue("@dificultad", selectedDifficulty);
                command.Parameters.AddWithValue("@minas", nMinas);
                command.Parameters.AddWithValue("@fecha", DateTime.Now);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
        #endregion

        #region button_Click
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Image myImage = new Image();
            myImage.Source = new BitmapImage(new Uri("img/bomb.png", UriKind.Relative));

            if (button.Content == "")
            {
                button.Content = "X";
                button.Foreground = Brushes.Red;
                timer.Stop();
                MessageBox.Show("Has perdido... Practica más.");

                // Desactiva todos los botones restantes
                foreach (Button btn in mainGrid.Children.OfType<Button>())
                {
                    if (btn.Content == "")
                    {
                        btn.Content = "X";
                        btn.IsEnabled = false;
                        btn.Foreground = Brushes.Red;

                    }
                    btn.IsEnabled = false;
                    startButton.IsEnabled = false;
                    difficultyComboBox.IsEnabled = true;

                }
            }
            else
            {
                int row = Grid.GetRow(button);
                int col = Grid.GetColumn(button);
                int count = CountMinesAroundButton(row, col);
                button.Content = count.ToString();
                if (count == 0)
                {
                    button.IsEnabled = false;
                    DiscoverEmptyCells(row, col);
                }
                else
                {
                    button.IsEnabled = false;
                }
                UpdateDiscoveredCells(_userViewModel);
                //MessageBox.Show(count.ToString());
            }

            button.IsEnabled = false;
        }
        #endregion

        #region setGridSize
        private void SetGridSize(string difficulty)
        {
            if (difficulty == "Fácil")
            {
                nRows = 8;
                nCols = 8;
            }
            else if (difficulty == "Intermedio")
            {
                nRows = 12;
                nCols = 12;
            }
            else if (difficulty == "Difícil")
            {
                nRows = 16;
                nCols = 16;
            }


        }
        #endregion

        #region button_PreviewMouseRightButtonDown
        private void Button_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Button button = (Button)sender;

            // Verifica si el contenido del botón es una cadena vacía
            if (button.Content == "")
            {
                // Si es así, agrega un nuevo contenido al botón
                button.Content = "🚩";
            }
            else if (button.Content == "🚩")
            {
                // Si el contenido es "?", elimina el contenido del botón
                button.Content = "";
            }

            // Cancela el comportamiento por defecto de hacer clic derecho
            e.Handled = true;
        }
        #endregion

        #region startButton_Click
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            // Reinicia el temporizador
            elapsed = TimeSpan.Zero;
            timeElapsedLabel.Content = "";
            // Elimina los botones anteriores
            mainGrid.Children.Clear();
            difficultyComboBox.IsEnabled = false;
            nMinas = 0;
            discoveredCells = 0;

            // Crea una matriz de nRows x nCols de botones
            mainGrid.RowDefinitions.Clear();
            mainGrid.ColumnDefinitions.Clear();
            mainGrid.Children.Clear();

            for (int i = 0; i < nRows; i++)
            {
                mainGrid.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < nCols; i++)
            {
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nCols; j++)
                {
                    Button button = new Button();
                    button.Name = "Button" + i + j;
                    button.Background = Brushes.White;
                    button.Click += Button_Click;
                    button.PreviewMouseRightButtonDown += Button_PreviewMouseRightButtonDown;
                    Grid.SetRow(button, i);
                    Grid.SetColumn(button, j);
                    mainGrid.Children.Add(button);
                    button.IsEnabled = false;

                }
            }

            foreach (Button button in mainGrid.Children.OfType<Button>())
            {
                button.IsEnabled = true;
            }

            // Llama al evento de cambio de selección de la caja de selección de dificultad
            difficultyComboBox_SelectionChanged(null, null);

            // Agrega nMinas aleatorias
            Random rand = new Random();
            for (int i = 0; i < (nMinas); i++)
            {
                int row = rand.Next(0, nRows);
                int col = rand.Next(0, nCols);

                Button button = (Button)mainGrid.Children
                    //método OfType para obtener una lista de todos los botones en el contenido de la ventana
                    .OfType<Button>()
                    //método FirstOrDefault para encontrar el primer botón que coincide con la fila y columna especificadas
                    .FirstOrDefault(b => Grid.GetRow(b) == row && Grid.GetColumn(b) == col);
                if (button != null)
                {
                    button.Content = "";
                    button.Background = Brushes.White;
                }
            }
            MessageBox.Show("Hay " + nMinas + " minas en este tablero...Suerte!");
            startButton.IsEnabled = false;
            timer.Start();
        }
        #endregion

        #region menuButton_Click
        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            var menuPage = new MenuPage(_userViewModel);
            NavigationService.Navigate(menuPage);
            //NavigationService.Navigate(new MenuPage());
        }
        #endregion
    }
}
