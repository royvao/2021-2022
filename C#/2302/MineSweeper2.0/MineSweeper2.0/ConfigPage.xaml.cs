﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace MineSweeper2._0
{

    public partial class ConfigPage : Page
    {
        #region components
        private readonly UserViewModel _userViewModel;
        string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Usuarios;user id = sa; password = mssqlserver";
        string clearQuery = "DELETE FROM estadisticas WHERE nombre = @usuario";
        #endregion
        public ConfigPage(UserViewModel userViewModel)
        {
            #region info
            InitializeComponent();
            Loaded += ConfigPage_Loaded;
            _userViewModel = userViewModel;
            #endregion

        }
        #region configPage_Loaded
        private void ConfigPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Width = 500;
            parentWindow.Height = 600;
        }
        #endregion

        #region exitButton_Click
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result =
               MessageBox.Show("¿Está seguro de que desea cerrar sesión?", "Cerrar sesión", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                NavigationService.Navigate(new LoginPage());
            }

        }
        #endregion

        #region modpwdButton_Click
        private void ModpwdButton_Click(object sender, RoutedEventArgs e)
        {
            var chngpwdPage = new ChangePasswordPage(_userViewModel);
            NavigationService.Navigate(chngpwdPage);
        }
        #endregion

        #region clearStatsButton_Click
        private void ClearStatsButton_Click(object sender, RoutedEventArgs e)
        {
            StatsPage statsPage = new(_userViewModel);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(clearQuery, connection);
                command.Parameters.AddWithValue("@usuario", _userViewModel.userName);
                connection.Open();
                int rowsAffected = command.ExecuteNonQuery();
                connection.Close();

                if (rowsAffected > 0)
                {
                    MessageBoxResult result =
                        MessageBox.Show("¿Está seguro de que desea borrar todas las estadísticas?", "Confirmar eliminación", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.Yes)
                    {
                        statsPage.statsGrid.Children.Clear();
                        MessageBox.Show("Las estadísticas se han eliminado.");
                    }
                    else
                    {
                        MessageBox.Show("No se han encontrado estadísticas para eliminar.");
                    }
                }
            }
        }
        #endregion

        #region backButton_Click
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            var menuPage = new MenuPage(_userViewModel);
            NavigationService.Navigate(menuPage);
        }
        #endregion
    }
}
