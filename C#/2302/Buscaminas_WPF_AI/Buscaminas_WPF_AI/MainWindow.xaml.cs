﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Buscaminas_WPF_AI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int[,] mineField = new int[ROWS, COLUMNS];
        private Button[,] buttons = new Button[ROWS, COLUMNS];
        private int minesToPlace = 5;
        private Random rnd;
        private int seed;
        private const int ROWS = 10;
        private const int COLUMNS = 10;
        private bool[,] mines = new bool[ROWS, COLUMNS];

        public MainWindow()
        {
            InitializeComponent();
            seed = (int)DateTime.Now.Ticks;
            rnd = new Random(seed);
        }

        private void boardSizeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem selectedItem = (ComboBoxItem)boardSizeComboBox.SelectedItem;
            string selectedSize = selectedItem.Content.ToString();
            string[] sizeParts = selectedSize.Split('x');
            int rows = int.Parse(sizeParts[0]);
            int columns = int.Parse(sizeParts[1]);

            // Redimensionar los arrays mineField, buttons y mines
            mineField = new int[rows, columns];
            buttons = new Button[rows, columns];
            mines = new bool[rows, columns];

            // Actualizar la UI
            MineField.Children.Clear();
            GenerateMines();
            mineField = CreateMineField();
            CreateButtons();
        }

        private void difficultyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem selectedItem = (ComboBoxItem)difficultyComboBox.SelectedItem;
            string selectedDifficulty = selectedItem.Content.ToString();
            //TODO: Generate a new minefield with the selected difficulty
            switch (selectedDifficulty)
            {
                case "Easy":
                    minesToPlace = 5;
                    break;
                case "Normal":
                    minesToPlace = 10;
                    break;
                case "Hard":
                    minesToPlace = 15;
                    break;
            }
            //TODO: Update the UI to reflect the new minefield
            MineField.Children.Clear();
            GenerateMines();
            mineField = CreateMineField();
            CreateButtons();
        }

        private void GenerateMines()
        {
            Random random = new Random();
            for (int i = 0; i < ROWS; i++)
            {
                for (int j = 0; j < COLUMNS; j++)
                {
                    if (i >= 0 && i < ROWS && j >= 0 && j < COLUMNS)
                    {
                        mines[i, j] = rnd.Next(2) == 1;
                    }
                }
            }
        }


        private int[,] CreateMineField()
        {
            int[,] mineField = new int[ROWS, COLUMNS];

            for (int i = 0; i < ROWS; i++)
            {
                for (int j = 0; j < COLUMNS; j++)
                {
                    mineField[i, j] = 0;
                }
            }

            while (minesToPlace > 0)
            {
                int x = rnd.Next(ROWS);
                int y = rnd.Next(COLUMNS);
                if (mineField[x, y] == 0)
                {
                    mineField[x, y] = 9;
                    minesToPlace--;
                }
            }

            return mineField;
        }

        private void CreateButtons()
        {
            for (int i = 0; i < ROWS; i++)
            {
                for (int j = 0; j < COLUMNS; j++)
                {
                    Button button = new Button();
                    button.Click += ButtonClick;
                    MineField.Children.Add(button);
                    Grid.SetRow(button, i);
                    Grid.SetColumn(button, j);
                    buttons[i, j] = button;
                }
            }
        }

        private int GetMineCount(int row, int column)
        {
            int count = 0;
            for (int i = row - 1; i <= row + 1; i++)
            {
                for (int j = column - 1; j <= column + 1; j++)
                {
                    if (i >= 0 && i < ROWS && j >= 0 && j < COLUMNS && (i != row || j != column))
                    {
                        if (mines[i, j])
                        {
                            count++;
                        }
                    }
                }
            }
            return count;
        }


        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            // Verifica si la celda ya ha sido revelada previamente
            if ((string)button.Content != string.Empty)
            {
                return;
            }

            int row = (int)button.GetValue(Grid.RowProperty);
            int column = (int)button.GetValue(Grid.ColumnProperty);
            int mineCount = GetMineCount(row, column);

            // Asigna el contenido de la celda solo si hay minas a su alrededor
            button.Content = mineCount > 0 ? mineCount.ToString() : string.Empty;
        }

    }
}
