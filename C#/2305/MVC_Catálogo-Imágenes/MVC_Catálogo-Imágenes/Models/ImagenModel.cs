﻿namespace MVC_Catálogo_Imágenes.Models
{
    public class ImagenModel
    {
        public int Id_Imagen { get; set; }
        public string? Nombre { get; set; }
        public string? Imagen { get; set; }
        public string? Fuente { get; set; }
    }
}
