﻿namespace MVC_Catálogo_Imágenes.Data
{
    public class DbContext
    {
        public DbContext(string valor) => Valor = valor;
        public string Valor { get; }
    }
}
