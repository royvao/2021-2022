﻿using System;
using System.Collections.Generic;

namespace IntroASP.Models;

public partial class UsersDataVM
{
    public int Id { get; set; }

    public string? UserName { get; set; }

    public string? UserPassword { get; set; }
}
