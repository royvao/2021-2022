﻿using CapaDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CN_Producto
    {
        private CD_Producto oCapaDato = new CD_Producto();
        public List<Producto> Listar()
        {
            return oCapaDato.Listar();
        }

        public int Registrar(Producto p, out string Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(p.Nombre) || string.IsNullOrWhiteSpace(p.Nombre))
            {
                Mensaje = "El nombre no puede estar vacío.";
            }
            else if (string.IsNullOrEmpty(p.Descripcion) || string.IsNullOrWhiteSpace(p.Descripcion))
            {
                Mensaje = "La descripción no puede estar vacía.";
            }

            else if(p.oMarca.IdMarca == 0)
            {
                Mensaje = "Debe seleccionar una marca.";
            }
            else if(p.oCategoria.IdCategoria == 0)
            {
                Mensaje = "Debe seleccionar una categoría.";
            }
            else if(p.oMarca.IdMarca == 0)
            {
                Mensaje = "Debe seleccionar una marca.";
            }
            else if (p.Precio == 0)
            {
                Mensaje = "El precio no puede ser cero (0).";
            }
            else if (p.Stock == 0)
            {
                Mensaje = "El stock no puede ser cero (0).";
            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return oCapaDato.Registrar(p, out Mensaje);
            }
            else
            {
                return 0;
            }

        }

        public bool Editar(Producto p, out string Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(p.Nombre) || string.IsNullOrWhiteSpace(p.Nombre))
            {
                Mensaje = "El nombre no puede estar vacío.";
            }
            else if (string.IsNullOrEmpty(p.Descripcion) || string.IsNullOrWhiteSpace(p.Descripcion))
            {
                Mensaje = "La descripción no puede estar vacía.";
            }

            else if (p.oMarca.IdMarca == 0)
            {
                Mensaje = "Debe seleccionar una marca.";
            }
            else if (p.oCategoria.IdCategoria == 0)
            {
                Mensaje = "Debe seleccionar una categoría.";
            }
            else if (p.oMarca.IdMarca == 0)
            {
                Mensaje = "Debe seleccionar una marca.";
            }
            else if (p.Precio == 0)
            {
                Mensaje = "El precio no puede ser cero (0).";
            }
            else if (p.Stock == 0)
            {
                Mensaje = "El stock no puede ser cero (0).";
            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return oCapaDato.Editar(p, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool GuardarDatosImagen(Producto p, out string Mensaje)
        {
            return oCapaDato.GuardarDatosImagen(p, out Mensaje);
        }

        public bool Eliminar(int id, out string Mensaje)
        {
            return oCapaDato.Eliminar(id, out Mensaje);
        }
    }
}
