﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Battleship_WPF
{
   public partial class SignupPage : Page
    {
        #region components
        string connectionString = @"Data Source=(local);Initial Catalog=Usuarios;Integrated Security=True";
        string queryString = "INSERT INTO usersData (userName, userPassword) VALUES (@usuario, @contraseña)";
        string checkQuery = "SELECT COUNT(*) FROM usersData WHERE userName = @userName";

        #endregion
        public SignupPage()
        {
            #region info
            InitializeComponent();
            Loaded += SignupPage_Loaded;
            #endregion        
        }

        #region signupPage_Loaded
        private void SignupPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Width = 450;
            parentWindow.Height = 550;
        }
        #endregion

        #region signupButton_Click
        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            var userViewModel = new UserViewModel();
            string userName = usernameTextBox.Text;
            string password = passwordBox.Password;
            string password2 = password2Box.Password;
            var random = new Random();
            int randomno = random.Next(0, 100);


            if (string.IsNullOrEmpty(userName))
            {
                usLbl.Content = "Debe ingresar un nombre de usuario.";
                pw1Lbl.Content = "";
                pw2Lbl.Content = "";
                return;
            }
            else if (string.IsNullOrEmpty(password))
            {
                pw1Lbl.Content = "Debe ingresar una contraseña.";
                usLbl.Content = "";
                pw2Lbl.Content = "";
                return;
            }
            else if (string.IsNullOrEmpty(password2))
            {
                pw2Lbl.Content = "Debe repetir la contraseña.";
                pw1Lbl.Content = "";
                usLbl.Content = "";
                return;
            }

            if (password == password2)
            {
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();

                // Verificar si el nombre de usuario ya existe en la base de datos
                SqlCommand checkCommand = new SqlCommand(checkQuery, connection);
                checkCommand.Parameters.AddWithValue("@userName", userName);
                int count = (int)checkCommand.ExecuteScalar();
                if (count > 0)
                {
                    usLbl.Content = "";
                    pw1Lbl.Content = "";
                    pw2Lbl.Content = "";
                    errorMessageTextBlock.Text = "El nombre de usuario ya existe.";
                    errorMessage2TextBlock.Text = "Prueba con @" + userName + randomno;
                    return;
                }
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@usuario", userName);
                command.Parameters.AddWithValue("@contraseña", password);
                command.ExecuteNonQuery();
                connection.Close();

                NavigationService.Navigate(new LoginPage());
            }
            else
            {
                usLbl.Content = "";
                pw1Lbl.Content = "";
                pw2Lbl.Content = "";
                errorMessageTextBlock.Text = "Las contraseñas no coinciden.";
            }

        }
        #endregion

        #region backButton_Click
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new LoginPage());
        }
        #endregion
    }
}
