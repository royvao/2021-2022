﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;

#region quitAdverts
// Quitar adevertencias
#pragma warning disable CS8600
#pragma warning disable CS0252
#pragma warning disable CS8622
#pragma warning disable CS8625
#pragma warning disable CS8618
#endregion

namespace Battleship_WPF
{
   
    public partial class GamePage : Page
    {
        #region components           
        int nBarcos;
        int nRows = 10;
        int nCols = 10;
        string selectedDifficulty;
        private TimeSpan elapsed;
        DispatcherTimer timer;
        private readonly UserViewModel _userViewModel;
        string connectionString = @"Data Source=(local);Initial Catalog=Usuarios;Integrated Security=True";
        #endregion

        public GamePage(UserViewModel userViewModel)
        {
            #region allGameInfo
            InitializeComponent();
            _userViewModel = userViewModel;
            startButton.IsEnabled = false;
            difficultyComboBox.IsEnabled = false;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            Loaded += GamePage_Loaded;
            #endregion
        }

        #region gamePage_Loaded
        private void GamePage_Loaded(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Width = 625;
            parentWindow.Height = 500;
        }
        #endregion

        #region timer_Tick
        private void Timer_Tick(object sender, EventArgs e)
        {
            // Incrementa el tiempo transcurrido en un segundo
            elapsed = elapsed.Add(TimeSpan.FromSeconds(1));

            // Verifica si el tiempo transcurrido es inferior a un minuto
            if (elapsed.TotalSeconds < 60)
            {
                // Muestra los segundos en el temporizador
                timeElapsedLabel.Content = "Tu tiempo: " + elapsed.Seconds.ToString("D2") + " segundos.";
            }
            else
            {
                // Muestra los minutos y segundos en el temporizador
                timeElapsedLabel.Content = "Tu tiempo: " + elapsed.ToString(@"m\:ss");
            }
        }
        #endregion

        #region difficultyComboBox_SelectionChanged
        private void difficultyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            startButton.IsEnabled = true;

            // Obtiene la selección del usuario
            if (difficultyComboBox.SelectedIndex == 0)
            {
                nBarcos = 10;
                selectedDifficulty = "Fácil";
                SetGridSize(selectedDifficulty);
            }
            else if (difficultyComboBox.SelectedIndex == 1)
            {
                nBarcos = 20;
                selectedDifficulty = "Intermedio";
                SetGridSize(selectedDifficulty);
            }
            else if (difficultyComboBox.SelectedIndex == 2)
            {
                nBarcos = 30;
                selectedDifficulty = "Difícil";
                SetGridSize(selectedDifficulty);
            }
        }

        #endregion              

        #region button_Click
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            
        }
        #endregion

        #region setGridSize
        private void SetGridSize(string difficulty)
        {
            if (difficulty == "Fácil")
            {
                nRows = 8;
                nCols = 8;
            }
            else if (difficulty == "Intermedio")
            {
                nRows = 12;
                nCols = 12;
            }
            else if (difficulty == "Difícil")
            {
                nRows = 16;
                nCols = 16;
            }


        }
        #endregion

        #region startButton_Click
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            // Reinicia el temporizador
            elapsed = TimeSpan.Zero;
            timeElapsedLabel.Content = "";

            // Elimina los botones anteriores
            mainGrid.Children.Clear();
                        
            difficultyComboBox.IsEnabled = false;
            nBarcos = 0;

            // Crea una matriz de nRows x nCols de botones
            mainGrid.RowDefinitions.Clear();
            mainGrid.ColumnDefinitions.Clear();
            mainGrid.Children.Clear();

            for (int i = 0; i < nRows; i++)
            {
                mainGrid.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < nCols; i++)
            {
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nCols; j++)
                {
                    Button button = new Button();
                    button.Name = "Button" + i + j;
                    button.Background = Brushes.White;
                    button.Click += Button_Click;
                    Grid.SetRow(button, i);
                    Grid.SetColumn(button, j);
                    mainGrid.Children.Add(button);
                    button.IsEnabled = false;
                }
            }

            foreach (Button button in mainGrid.Children.OfType<Button>())
            {
                button.IsEnabled = true;
            }

            // Llama al evento de cambio de selección de la caja de selección de dificultad
            difficultyComboBox_SelectionChanged(null, null);
            
            startButton.IsEnabled = false;
            timer.Start();
        }
        #endregion

        #region menuButton_Click
        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            var menuPage = new MenuPage(_userViewModel);
            NavigationService.Navigate(menuPage);
            //NavigationService.Navigate(new MenuPage());
        }
        #endregion

        #region modeComboBox_SelectionChanged
        private void ModeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            difficultyComboBox.IsEnabled = true;

            // Obtiene la selección del usuario
            if (difficultyComboBox.SelectedIndex == 0)
            {
                selectedDifficulty = "Multijugador";
            }
            else if (difficultyComboBox.SelectedIndex == 1)
            {
                selectedDifficulty = "Solitario";
            }
        }
        #endregion
    }
}
