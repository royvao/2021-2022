﻿namespace SocketCliente
{
    partial class Seleccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAmigo = new System.Windows.Forms.TextBox();
            this.btnChatear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtAmigo
            // 
            this.txtAmigo.Location = new System.Drawing.Point(121, 75);
            this.txtAmigo.Name = "txtAmigo";
            this.txtAmigo.Size = new System.Drawing.Size(100, 20);
            this.txtAmigo.TabIndex = 0;
            // 
            // btnChatear
            // 
            this.btnChatear.Location = new System.Drawing.Point(137, 101);
            this.btnChatear.Name = "btnChatear";
            this.btnChatear.Size = new System.Drawing.Size(75, 23);
            this.btnChatear.TabIndex = 1;
            this.btnChatear.Text = "Chatear";
            this.btnChatear.UseVisualStyleBackColor = true;
            this.btnChatear.Click += new System.EventHandler(this.btnChatear_Click);
            // 
            // Seleccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 227);
            this.Controls.Add(this.btnChatear);
            this.Controls.Add(this.txtAmigo);
            this.Name = "Seleccion";
            this.Text = "Seleccion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtAmigo;
        private System.Windows.Forms.Button btnChatear;
    }
}