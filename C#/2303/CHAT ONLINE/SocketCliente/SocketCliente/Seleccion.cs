﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketCliente
{
    public partial class Seleccion : Form
    {
        string nick;
        Dictionary<string, Chat> di;
        Conexion con;
        public Seleccion(string nick, Dictionary<string, Chat> di, Conexion con)
        {
            InitializeComponent();
            this.nick = nick;
            this.di = di;
            this.con = con; 
        }

        private void btnChatear_Click(object sender, EventArgs e)
        {
            if(txtAmigo.Text != "")
            {
                Chat c = new Chat(this.nick, txtAmigo.Text, this.con);
                c.Show();
                di.Add(txtAmigo.Text, c);
            }
            else
            {
                MessageBox.Show("Escriba el nick del amigo");
            }
        }
    }
}
