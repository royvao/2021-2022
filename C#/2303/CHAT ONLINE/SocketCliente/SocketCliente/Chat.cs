﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketCliente
{
    public partial class Chat : Form
    {
        private string nick;
        private string amigo;
        private Conexion con;

        public Chat(string nick, string amigo, Conexion con)
        {
            InitializeComponent();
            this.nick = nick;
            this.amigo = amigo;
            this.con = con;
            lblChateando.Text += this.amigo;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            if(txtMensaje.Text != "")
            {
                this.con.send(this.amigo + "-" + txtMensaje.Text);
                txtChat.Text += this.nick + ": " + txtMensaje.Text + "\r\n";
                txtMensaje.Text = "";
            }
        }
        public void agregar(string mensaje)
        {
            txtChat.Text = mensaje + "\r\n";
        }
    }
}
