﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;


class Program
{
    // Lista de clientes conectados
    static List<TcpClient> clientes = new List<TcpClient>();
    static async Task Main(string[] args)
    {
        TcpListener tcpListener;
        Program programa = new Program();
        try
        {
            // Crear el punto de conexión para escuchar
            IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
            IPEndPoint localEndPoint = new(ipAddress, 8080);

            // Crear el socket TCP para escuchar
            tcpListener = new(localEndPoint);

            // Iniciar la escucha del socket
            tcpListener.Start();

            Console.WriteLine("Servidor iniciado y escuchando en el puerto 8080...");

            while (true)
            {
                TcpClient cliente =  tcpListener.AcceptTcpClient();

                // Iniciar un hilo para manejar la conexión del cliente
                await Task.Run(() => programa.HandleClient(cliente));
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    #region HANDLE CLIENT
    public async Task HandleClient(TcpClient client)
    {
        await using NetworkStream stream = client.GetStream();

        try
        {
            #region 1
            // Agregar el cliente a la lista de clientes conectados
            clientes.Add(client);
            Console.WriteLine($"Cliente {clientes.Count} añadido.");

            // Leer mensajes del cliente
            byte[] buffer = new byte[1024];
            int bytesRead = await stream.ReadAsync(buffer);
            string message = Encoding.ASCII.GetString(buffer, 0, bytesRead);
            Console.WriteLine($"- Cliente {clientes.Count}: {message}");

            // Verificar si hay dos clientes conectados
            if (clientes.Count == 2)
            {
                Console.WriteLine("Ambos clientes conectados.");
                Console.WriteLine("Preprados para jugar...");
            }
            else
            {
                Console.WriteLine("Esperando a que se conecte otro cliente...");
            }

            #endregion
        }
        catch (Exception e)
        {
            Console.WriteLine($"Error en el cliente: {e.Message}");

            // Eliminar el cliente de la lista de clientes
            clientes.Remove(client);

            // Cerrar la conexión con el cliente
            client.Close();
        }
    }
    #endregion

    #region LISTO
    public async Task Listo(string user, TcpClient cliente)
    {
        try
        {
            while (clientes.Count != 2)
            {
                Console.WriteLine("No hay otro cliente conectado.");
            }

            // Si ambos clientes están disponibles, enviar el mensaje
            NetworkStream stream = cliente.GetStream();
            byte[] mensajeBytes = Encoding.ASCII.GetBytes(user + " está listo.");
            await stream.WriteAsync(mensajeBytes, 0, mensajeBytes.Length);
            await Task.Run(() => Receive(cliente));

            Console.WriteLine("Mensaje enviado al oponente.");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
    #endregion

    #region REMOVE
    public async Task Remove()
    {
        while (clientes.Count != 0)
        {
            Console.WriteLine($"El cliente{clientes.Count} se ha desconectado");
            clientes.Clear();
        }
    }
    #endregion

    #region RECEIVE
    public async Task Receive(TcpClient client)
    {
        try
        {
            NetworkStream stream = client.GetStream();
            // Leer mensajes del servidor
            byte[] buffer = new byte[1024];
            int bytesRead = await stream.ReadAsync(buffer);
            string message = Encoding.ASCII.GetString(buffer, 0, bytesRead);
            Console.WriteLine($"- Cliente: {message}");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());

        }
    }
    #endregion


}
