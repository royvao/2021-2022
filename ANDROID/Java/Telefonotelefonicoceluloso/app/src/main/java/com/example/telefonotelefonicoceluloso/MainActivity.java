package com.example.telefonotelefonicoceluloso;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText et1, et2;
    private TextView tv1;
    private RadioButton rb1, rb2, rb3, rb4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText) findViewById(R.id.txtValor1);
        et2 = (EditText) findViewById(R.id.txtValor2);
        tv1 = (TextView) findViewById(R.id.txtResultant);
        rb1 = (RadioButton) findViewById(R.id.rbSuma);
        rb2 = (RadioButton) findViewById(R.id.rbRest);
        rb3 = (RadioButton) findViewById(R.id.rbMultiplication);
        rb4 = (RadioButton) findViewById(R.id.rbDivider);

    }

    public void calculator(View view){
        String valor1String = et1.getText().toString();
        String valor2String = et2.getText().toString();

        int valor1Int = Integer.parseInt(valor1String);
        int valor2Int = Integer.parseInt(valor2String);

        if(rb1.isChecked()){
            int suma = valor1Int + valor2Int;
            String resultant = String.valueOf(suma);
            tv1.setText(resultant);
        }else if(rb2.isChecked()){
            int rest = valor1Int - valor2Int;
            String resultant = String.valueOf(rest);
            tv1.setText(resultant);
        }else if(rb3.isChecked()) {
            int multiplication = valor1Int * valor2Int;
            String resultant = String.valueOf(multiplication);
            tv1.setText(resultant);
        }else if(rb4.isChecked()) {
            int divider = valor1Int / valor2Int;
            String resultant = String.valueOf(divider);
            tv1.setText(resultant);
        }
    }

}