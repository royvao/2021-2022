package com.example.nullsafety

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nullSafety()
    }

    fun nullSafety(){

        val myString = "Codisys"
        // myString = null  <- ERROR DE COMPILACION
        println(myString)

        // Variable null safety
        var mySafetyString: String? = "Codisys null safety"
        mySafetyString = null
        println(mySafetyString)

        mySafetyString = "Rate us!"
        println(mySafetyString)

        /* if (mySafetyString != null) {
            println(mySafetyString)
        }else{
            println(mySafetyString)
        }*/

        // Safe call
        println(mySafetyString?.length)

        mySafetyString?.let {
            println(it)
        } ?: run {
            println(mySafetyString)
        }
    }
}