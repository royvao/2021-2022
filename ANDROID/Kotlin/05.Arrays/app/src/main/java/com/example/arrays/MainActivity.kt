package com.example.arrays

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        arrays()
    }

    fun arrays(){

        val name = "Roy"
        val surname = "Vao"
        val company = "Codisys"
        val age = "23"

        // Crear Array

        val myArray : ArrayList<String> = arrayListOf<String>()

        // Añadir los datos de uno en uno

        myArray.add(name)
        myArray.add(surname)
        myArray.add(company)
        myArray.add(age)
        //myArray.add(age)
        //myArray.add(age)

        println(myArray)

        // Añadir conjunto de datos

        myArray.addAll(listOf("Hello!", "Welcome to TinPet."))

        println(myArray)

        // Acceso a datos

        val myCompany : String = myArray[2]

        println(myCompany)

        // Modificación de datos

        myArray[5] = "Rate the app with 5 stars!"

        println(myArray)

        // Eliminar datos

        myArray.removeAt(4)

        println(myArray)

        // Recorrer datos

        myArray.forEach{
            println(it)
        }

        // Otras operaciones

        println(myArray.count())

        myArray.clear()

        println(myArray.count())

        myArray.first()
        myArray.last()

        myArray.sort()
    }
}