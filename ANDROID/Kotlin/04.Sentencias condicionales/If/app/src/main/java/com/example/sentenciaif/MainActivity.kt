package com.example.sentenciaif

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sentenciaIf()
    }

    private fun sentenciaIf() {

        val myNumber = 2

        /* Operadores condicionales */
        // > mayor que
        // < menor que
        // >= mayor o igual que
        // <= menor o igual que
        // == igualdad
        // != desigualdad

        /* Operadores lógicos */
        // && operador "y"
        // || operador "o"
        // ! operador "no"

        if (!(myNumber <= 10 && myNumber > 5) || myNumber == 53) {
            // Sentencia if
            println("$myNumber es menor o igual que 10 y mayor que 5 o igual a 53")
        } else if (myNumber == 60) {
            // Sentencia else if
            println("$myNumber es igual a 60")

        } else if (myNumber != 70) {
            // Sentencia else if
            println("$myNumber no es igual a 70")

        } else {
            // Sentencia else
            println("$myNumber es mayor que 10 o menor o igual que 5 y distinto a 53")
        }

    }
}