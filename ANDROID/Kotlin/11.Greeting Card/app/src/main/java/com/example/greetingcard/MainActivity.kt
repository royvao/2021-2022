package com.example.greetingcard

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import com.example.greetingcard.ui.theme.GreetingCardTheme
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            GreetingCardTheme {
                InternetPermissionHandler()
            }
        }
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun InternetPermissionHandler() {
    val internetPermissionState = rememberPermissionState(android.Manifest.permission.CAMERA)

    LaunchedEffect(key1 = true){
        internetPermissionState.launchPermissionRequest()
    }

    Text(text = "Notification Screen")

    /*LaunchedEffect(key1 = true) {
        internetPermissionState.launchPermissionRequest()
    }*/

}

