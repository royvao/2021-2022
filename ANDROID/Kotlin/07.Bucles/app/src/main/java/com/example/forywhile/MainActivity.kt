package com.example.forywhile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loops()
    }

    fun loops(){

        /* Bucles */

        val myArray = listOf("Hello!", "Welcome to TinPet.", "Rate us.")
        val myMap = mutableMapOf("Roy" to 1, "Lidia" to 2, "Joel" to 5)

        // For

        for (myString in myArray)
            println(myString)

        for (myElement in myMap)
            println("${myElement.key}-${myElement.value}")

        for (x in 0..10)
            println(x)

        for (x in 9 until 30)
            println(x)

        for (x in 0..10 step 2)
            println(x)

        for (x in 10 downTo 0 step 3)
            println(x)

        val myNumericArray = (0..20)
        for (myNum in myNumericArray)
            println(myNum)

        // While

        var x = 0

        while (x < 10){
            println(x)
            x += 2
        }
    }
}
