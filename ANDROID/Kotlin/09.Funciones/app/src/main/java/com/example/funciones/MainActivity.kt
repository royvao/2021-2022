package com.example.funciones

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        funciones()
    }

    fun funciones(){
        sayHello()
        sayHello()
        sayHello()

        sayMyName("Roy")
        sayMyName("Lidia")
        sayMyName("Joel")

        sayMyNameAndAge("Luis", 50)
        sayMyNameAndAge("Lucía", 25)
        sayMyNameAndAge("Pedro", 78)

        val sumResult = sumTwoNumbers(5,6)
        println(sumResult)

        println(sumTwoNumbers(15,10))

        println(sumTwoNumbers(10,sumTwoNumbers(5,5)))

        sayMyNameAndAge(sayMyName("Roy"),sumTwoNumbers(sumTwoNumbers(5,5),sumTwoNumbers(7,6)))
    }

    // Función simple
    fun sayHello(){
        println("Hello, world!")
    }

    // Función con un parámetro de entrada
    fun sayMyName(name: String) : String{
        return name
    }

    // Función con un parámetro de entrada
    fun sayMyNameAndAge(name: String, age: Int){
        println("Hello, my name is $name y and im $age!")
    }

    // Función con valor de retorno
    fun sumTwoNumbers(firstNumber: Int, secondNumber: Int) : Int{
        val sum = firstNumber + secondNumber
        return sum
    }
}