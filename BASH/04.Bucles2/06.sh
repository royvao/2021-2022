#!/bin/bash

//se calcula el número de ficheros
f=$(find -type f | wc -l)
//se calcula el número de directorios
df=$(find -type d | wc -l)
//se resta el número de directorios menos 1 para eliminar el "./"
d=`expr $df - 1`

echo Hay un total de $f ficheros y de $d directorios

