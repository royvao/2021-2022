#!/bin/bash
operando_Uno=$1
operando_Dos=$2

echo "Selecciona un tipo de operación: "
echo "1- Suma"
echo "2- Resta"
echo "3- Multiplicación"
echo "4- División"
read operando

case $operando in
1)resultado=`echo $operando_Uno + $operando_Dos | bc`
;;
2)resultado=`echo $operando_Uno - $operando_Dos | bc`
;;
3)resultado=`echo $operando_Uno \* $operando_Dos | bc`
;;
4)resultado=`echo "scale=2; $operando_Uno / $operando_Dos" | bc`
;;
esac
echo "El resultado de su operación es: $resultado"

