#!/bin/bash

echo Introduce un número
read userNumber

//si la variable es menor que 15
if [ $userNumber -lt 15 ]; then
	
	//hasta que la variable sea igual a 16
	until [ $userNumber -eq 16 ]
	do
		echo El número es $userNumber
		let "userNumber++" //incrementamos
	done
else
	//hasta que la variable sea igual a 14
	until [ $userNumber -eq 14 ]
	do
		echo El número es $userNumber
		let "userNumber--" //decrementamos
	done
fi
