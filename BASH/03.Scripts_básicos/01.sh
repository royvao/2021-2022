#!/bin/bash

//guardamos en una variable la salida del comando date (pero sólo el día actual)
today=$(date +%d)

//si el día actual es 29

if [ $today -eq 29 ]; then
	echo Es final de mes
else 
	echo No es final de mes
fi
