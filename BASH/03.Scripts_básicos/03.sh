#!/bin/bash

echo Introduce un fichero:
read fichero

//si la variable es un fichero 
if [ -f $fichero ]; then
	mv $fichero test //movemos el fichero a una carpeta
	echo El fichero ha sido movido a la carpeta test
else
	touch $fichero //si no, creamos el fichero
	mv $fichero test //y lo movemos a la carpeta
	echo Se ha creado el fichero $fichero y ha sido movido a la carpeta test
fi
