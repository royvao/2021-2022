#!/bin/bash

//guardamos en una variable el número de líneas de un fichero
fileOne=`wc --lines < 01.sh`
fileTwo=`wc --lines < 02.sh`

//si un fichero tiene más líneas que otro
if [ $fileOne -gt $fileTwo ];then
	echo El primer fichero tiene mayor cantidad de líneas
else
	echo El segundo fichero tiene mayor cantidad de líneas
fi
