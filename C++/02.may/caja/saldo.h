#ifndef SALDO_H
#define SALDO_H

#include "caja.h"

class Saldo:public CajaFuerte{
	private:
		unsigned int total;
	public:
		Saldo(int total);
		int consultarSaldo(int _total);		
		
};
#endif // SALDO_H
