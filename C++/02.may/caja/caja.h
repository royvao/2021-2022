#ifndef CAJAFUERTE_H
#define CAJAFUERTE_H

class CajaFuerte {
  private:
    int clave;
    int saldo;
  public:
    CajaFuerte(int _clave, int _saldo);
    bool comprobarClave(int pin);
    //int  meterDinero(int dinero);
    //int  sacarDinero(int dinero);
    int consultarSaldo();
    //int  numero();
};

#endif // CAJAFUERTE_H
