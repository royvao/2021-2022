/* ESTE PROGRAMA HA SIDO EXTRAÍDO ÍNTEGRAMENTE DE: https://gitlab.com/txemagon/c-programs */

#include <cstdlib>
#include <iostream>
#include <vector>

#define N 5

using namespace std;

void inspect (const vector<unsigned> &vec, const char *title = "") {

    vector<unsigned>::const_iterator it;

    cout << "Vector: "<< title << " \n" <<
            "=======\n";
    cout << "Capacity: " << vec.capacity ()                       << "\n";
    cout << "Empty:    " << (vec.empty 	 () ?  "True" : "False")  << "\n";
    cout << "Size:     " << vec.size  	 ()    			  << "\n";
    cout << "Front:    " << vec.front 	 ()    			  << "\n";
    cout << "Last:     " << vec.back  	 ()    			  << "\n";
    
    cout << "Vector: [ ";
    for (it=vec.begin (); it!=vec.end(); it++)
        cout << *it << " ";
    cout << "]\n\n\n";
}

void push (vector<unsigned> *v, unsigned val) {
    v->push_back (val);
    inspect (*v, "push");
}

unsigned pop (vector<unsigned> *v) {
    unsigned d = v->back ();
    cout << "Pop data: " << d <<"\n";
    v->pop_back ();
    inspect (*v, "Pop");
    return d;
}

unsigned next_fibo () {
  static unsigned prev = 1, prevprev = 1;
  unsigned curr = prev + prevprev;

  prevprev = prev;
  prev = curr;

  return curr;
}

int main (int argc, char *argv[]) {

    vector<unsigned> fibo;

    fibo.reserve (10);
    inspect (fibo);

    // emplace_back no crea un objeto temporal. Buscar ejemplo.
    fibo.emplace_back (1);
    fibo.push_back (1);

    for (unsigned i=0; i<N; i++)
        push (&fibo, next_fibo ());

    for (unsigned i=0; i<N/2; i++)
        pop (&fibo);

    /* Get a pointer to data */
    unsigned *data = fibo.data ();
    cout << "\n";
    for (unsigned i=0; i<fibo.size (); i++)
        cout << *(data + i) << " ";
    cout << "\n";

    cout << "Position 3: " << fibo[3] << "\n";

    /* Clearing */
    push (&fibo, next_fibo ());
    fibo.clear ();
    inspect (fibo, "Cleared");

    return EXIT_SUCCESS;
}

