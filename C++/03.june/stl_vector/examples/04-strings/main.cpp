#include <iostream>
#include <vector>

using namespace std;

int main(){
	vector<string> vect; // CREAMOS UN VECTOR DE STRINGS
	vect.push_back("Texto de prueba"); // AÑADIMOS A LA FUERZA UN STRING

	cout << vect[0] << endl; // IMPRIMIMOS LA PRIMERA CELDA DEL VECTOR

	return 0;
}
