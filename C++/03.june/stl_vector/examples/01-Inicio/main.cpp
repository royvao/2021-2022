#include <vector>
#include <iostream>

int main(){
	std::vector<int> vect = {1, 2, 3}; // DECLARAS EL VECTOR DE TIPO int CON 3 VALORES
	vect.push_back(40); // EL VECTOR PERMITE AÑADIR ELEMENTOS SIN LÍMITE

	/* IMPRIMIMOS EL VALOR DE CADA CELDA DEL VECTOR */

	std::cout << "La celda 1 contiene: " << vect[0] << "\n";	
	std::cout << "La celda 2 contiene: " << vect[1] << "\n";	
	std::cout << "La celda 3 contiene: " << vect[2] << "\n";	

	std::cout << "El tamaño es: " << vect.size() << "\n"; // CON EL METODO size(), NOS DEVUELVE EL TAMAÑO DEL VECTOR

	return 0;
}
