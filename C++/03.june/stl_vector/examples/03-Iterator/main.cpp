#include <vector>
#include <iostream>

int main(){
	std::vector<int> vect; // DECLARAS EL VECTOR DE TIPO int SIN UN TAMAÑO
	int num;

	std::cout << "Cuántos números quieres en el vector: ";
	std::cin >> num;

	for (int i=0; i< num; i++){
		int numero;
		std::cout << i+1 << "º número: ";
		std::cin >> numero;

		vect.push_back(numero); // PODEMOS METER SIN PROBLEMA CADA NUMERO EN EL VECTOR PORQUE NO TIENE LÍMITE
	}

	std::cout << "\n";
	
	/* FORMA DE HACERLO EN C++*/

	for (std::vector<int>::iterator it = vect.begin(); it != vect.end(); it++ ) // ITERATOR ES EL ELEMENTO UTILIZADO PARA DAR "VUELTAS" A UN ARRAY
	/* MIENTRAS EL PUNTO DONDE SE INICIA EL VECTOR (begin()) NO SEA EL PUNTO DONDE ACABA (end()), AUMENTAMOS UNA POSICIÓ*/
	{
		std::cout << *it /* PARA COGER EL VALOR*/ << "\n"; 

	}
	
	std::cout << "\n";
	
	/* FORMA DE HACERLO DESDE C++11*/
	
	for (auto it = vect.begin(); it != vect.end(); it++)
		std::cout << *it << "\n";
	
	std::cout << "\n";

	/* FORMA MÁS SIMPLIFICADA */

	for (int element : vect)
		std::cout << element << "\n";

	return 0;
}
