#include <iostream>
#include "static.h"

#define N 10
using namespace std;

int main(){
	Suma op[N]; // Creamo el objeto op con un array

	for (int i=0; i<N; i++)
		cout << "El objeto número: " << op[i].getAumentar() << " se ha creado\n";

	cout << "El número de objetos es: " << Suma::getN_objetos() << "\n";
	cout << "El resultado de sumar 2 y 3 es: " << Suma::sumar(2,3) << "\n"; // PRIMERA FORMA DE HACER LA SUMA
	cout << "El resultado de sumar 2 y 3 es: " << op[1].sumar(2,3) << "\n"; // OTRA FORMA DE HACER LA SUMA
	return 0;
}
