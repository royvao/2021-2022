#include <iostream>
#include "static.h"

Suma::Suma(){
	     this->aumentar=Suma::nobjetos++; // En cada llamada se aumenta 1.
}

int Suma::getAumentar(){
	return this->aumentar;
}

/* MÉTODO STATIC */
double Suma::sumar(int op1, int op2){
	return op1+op2;
}

int Suma::getN_objetos(){
	return Suma::nobjetos;
}
