#ifndef STRING_H
#define STRING_H

#include <iostream>

using namespace std;

class String{
	private:
		string nombre;
		string contraseña;
	public:
		String(string, string); // Método constructor con los tipos de dato de cada atributo
		
		/* Métodos get y set para el nombre */
		string getNombre();
	       	void setNombre(string);	

		/* Métodos get y set para la contraseña */
		string getContraseña();
		void setContraseña(string);
};
		
#endif // STRING_H
