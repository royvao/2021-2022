#ifndef HERENCIAS_H
#define HERENCIAS_H
#include <iostream>

class Coche{
	private:
		std::string marca;
	protected:
		unsigned int nruedas;
	public:
		virtual void acelerar()=0; //método virtual puro, se usa para las herencias
		Coche(std::string _marca, unsigned int _nruedas);
		std::string getMarca();
		void setMarca(std::string);
		
		unsigned int getNruedas();
		void setNruedas(unsigned int);

};

#endif //HERENCIAS_H
