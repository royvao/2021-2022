#ifndef TABLERO_H
#define	TABLERO_H 

#include <iostream>
using namespace std;

class Tablero{
	private:
		const Pieza * colocacion;
		vector <unsigned int> piezas_activas;
	public:
		bool set_pieza (Pieza* nuevaPieza, Posicion posicion);
		Posicion get_posicion (unsigned int id);
		Posicion get_posicion (const Pieza * pieza);

		Tablero (const Pieza * colocacion, vector<unsigned int> piezas_activas);

};
#endif // TABLERO_H 
