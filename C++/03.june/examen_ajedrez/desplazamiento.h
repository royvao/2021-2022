#ifndef DESPLAZAMIENTO_H
#define DESPLAZAMIENTO_H

class Desplazamiento{
	private:
		int filas;
		int columnas;
	public:
		Desplazamiento (int filas, int columnas);
		Desplazamiento()=delete;
		int get_filas();
		int get_columnas();
};

#endif // DESPLAZAMIENTO_H 
