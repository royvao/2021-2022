#ifndef VECTOR_H
#define VECTOR_H

class Vector{
	public:
		double x;
		double y;
		double z;
		
		Vector (double x, double y, double z);
		Vector operator+ (const Vector &op); 
};

#endif // VECTOR_H
