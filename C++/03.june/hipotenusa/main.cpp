#include <iostream>

#include "lado.h"
using namespace std;

double op(){
	int n;
	static int i=0;

	i++;
	cout << "Introduzca su " << i << "º operando: ";
	cin >> n;

	return n;
}

int main(){
	double cateto1;
	double cateto2;

	/* definimos variables */
	cateto1=op();
	cateto2=op();

	/*objetos*/
	Triangulo Cateto1;
	Triangulo Cateto2;
	
	Triangulo Hipotenusa;

	/* atributo privado -> modificamos valor del atributo del objeto cateto*/
	Cateto1.setCateto(cateto1);
	Cateto2.setCateto(cateto2);

	Hipotenusa.setCateto(Cateto1+Cateto2);

	cout << "La hipotenusa es: " << Hipotenusa.getCateto() << "\n";

	return 0;
}

