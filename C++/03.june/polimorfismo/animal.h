#ifndef ANIMAL_H
#define ANIMAL_H

#include <iostream>

using namespace std;

class Animal{
	protected:
		unsigned int npatas;
		unsigned int nojos;
		string nombre;
	public:
		virtual void dieta()=0; // Creamos un método virtual puro
};

#endif // ANIMAL_H
