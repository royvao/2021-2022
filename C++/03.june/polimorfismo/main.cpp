#include <iostream>
#include "carnivoro.h"

using namespace std;

int main(){
	/* POLIMORFISMO */
	Animal *animal[3];
	animal[0] = new Carnivoro(4,2,"bombón"); 
	animal[1] = new Carnivoro(6,12,"cartuchos");
	animal[2] = new Carnivoro(18,24,"cipote");

	animal[0]->dieta();
	animal[1]->dieta();
	animal[2]->dieta();
	
	return 0;
}

