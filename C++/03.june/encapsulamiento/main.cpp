#include <iostream>
#include "encapsulamiento.h"

int main(){
	encapsulamiento persona("roy",23);
	persona.getEdad();
	persona.setEdad(21);

	persona.getNombre();
	persona.setNombre("Juan");

	std::cout << "mi nombre es " << persona.getNombre() << " y tengo " << persona.getEdad() << " años" << "\n";
	
	return 0;
}


