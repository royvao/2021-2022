#ifndef ENCAPSULAMIENTO_H
#define ENCAPSULAMIENTO_H

#include <iostream>

class encapsulamiento{
	private:
		std::string nombre;
		unsigned int edad;
	public:
		encapsulamiento(std::string,unsigned int);
		encapsulamiento()=delete;

		std::string getNombre();
		void setNombre(std::string);

		unsigned int getEdad();
		void setEdad(unsigned int);
};


#endif
