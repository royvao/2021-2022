#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

void manejadora(int signal){
	
	for (int v=0;v<50;v+2){
		printf ("%i\n", v);
	}	 
}

void manejadora2(int signal2){
	printf("adios");
}

int main (int argc, char *argv[]){

	struct sigaction new;
	struct sigaction old;

	new.sa_handler=&manejadora;
	sigemptyset(&new.sa_mask);
	

	sigaction(SIGINT,&new,&old);
	
	while(1){
		sleep(2);
		printf("\n");
	}
	

	return 0;
}
