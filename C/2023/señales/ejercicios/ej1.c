#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

void manejadora(int signal){
	printf("He tratado la señal\n");

}

int main (int argc, char *argv[]){

	struct sigaction new;
	struct sigaction old;

	new.sa_handler=&manejadora;
	sigemptyset(&new.sa_mask);


	sigaction(SIGINT,&new,&old);
	
	while(1){
		sleep(2);
		printf("Estoy esperando una señal\n");
	}

	return 0;
}
