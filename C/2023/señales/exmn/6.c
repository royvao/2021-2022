#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

char *sgnl[]={"SIGHUP","SIGINT","SIGQUIT","SIGILL","SIGTRAP","SIGABRT","SIGBUS","SIGFPE","SIGKILL","SIGUSR1","SIGUSR2"};

void manejadora(int sig){
	printf("Proceso %i: señal %i recibida. Nombre de la señal - %s\n",getpid(), sig, sgnl[sig]);
}

int main(int argc, char *argv[]){
	
	signal(SIGINT, &manejadora);
	signal(SIGILL, &manejadora);
	signal(SIGTERM, &manejadora);
	signal(SIGUSR1, &manejadora);
	signal(SIGUSR2, &manejadora);

	while(1){
		sleep(2);
		printf("Estoy esperando una señal\n");
	}
	return 0;
}
