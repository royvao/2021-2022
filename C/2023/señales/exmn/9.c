#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

int x=0;
pid_t pid_hijo=0;

void manejadora(int signal){
	printf ("Señal recibida. Att: Hijo.\n");
	if(x==2){
		kill(getppid(), SIGINT);
		exit(-1);
	}else{
		x++;
	}
}

void manejadora2(int signal){
	kill(pid_hijo, SIGUSR1);
	printf("Envio señal al hijo\n");
}

int main(int argc, char *argv[]){
	pid_t pid;
	pid=fork();
	int status;

	if (pid==-1){
		printf("Error\n");
	}else{
		if(pid==0){
			//hijo
			signal(SIGUSR1, &manejadora);
			while(1){
				printf("Esperando la  señal de mi padre\n");
				sleep(3);
			}
		}else{
			//padre
			pid_hijo=pid;
			signal(SIGALRM, &manejadora2);
			while(1){
				alarm(5);
				pause();
			}
			wait(&status);		
		
		}

	}
	return 0;
}
