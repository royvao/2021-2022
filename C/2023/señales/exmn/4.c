#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

void manejadora(int signal){
	alarm(10);
}

void manejadora2(int signal){
	printf ("ADIOS\n");
}

int main(int argc, char *argv[]){
	struct sigaction new;
	struct sigaction old;
	struct sigaction new2;
	struct sigaction old2;

	new.sa_handler=&manejadora;
	sigemptyset(&new.sa_mask);

	new2.sa_handler=&manejadora2;
	sigemptyset(&new2.sa_mask);

	sigaction(SIGINT, &new, &old);
	sigaction(SIGALRM, &new2, &old2);

	while(1){
		sleep(2);
		printf("Estoy esperando una señal\n");
	}
	return 0;
}
