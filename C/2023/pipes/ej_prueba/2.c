#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>

void print_err(char * estr){
        perror(estr);
        exit(-1);
}

int main(){
        int ret = 0;
        // [0] Leer el descriptor de archivo
        // [1] escribir descriptor de archivo
        int pipefd[2] = {0};     // Leer y escribir descriptores de archivo utilizados para almacenar la canalización
        ret = pipe(pipefd);

        if(ret == -1)print_err("pipe fail");
	        ret = fork();
        	if(ret>0){
	                while(1){
		                sleep(1);
                	}
        	}else if(ret == 0){
	                while(1){
		                char buffer[20] = {0};
                		bzero(buffer,sizeof(buffer));
		                read(pipefd[0],buffer,sizeof(buffer) - 1);
                		printf("child recv data:%s\n",buffer);
                	}
        	}
	
	return 0;
}
