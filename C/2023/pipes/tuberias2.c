#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>

#define SIZE 512

int main (int argc, char *argv[]){
	
	pid_t pid;
	int padre[2], hijo[2];
	char buffer[SIZE], buffer_hijo[SIZE];

	pipe(padre);
	pipe(hijo);

	pid=fork();

	if(pid == -1){
		printf("ERROR\n");
	}else{
		if(pid == 0){
			//hijo
			close(padre[1]);
			close(hijo[0]);
			while(read(padre[0], buffer, SIZE)>0)
				printf("%s\n", buffer);
				close(padre[0]);
				strcpy(buffer_hijo, "SOY EL HIJO Y ESCRIBO AL PADRE\n");
				write(hijo[1], buffer_hijo, strlen(buffer_hijo));
				close(hijo[1]);
		}else{
			//padre
			close(padre[0]);
			strcpy(buffer, "Soy una tubería bidireccional OwO\n");
			write(padre[1], buffer, sizeof(buffer));
			close(padre[1]);

			close(hijo[1]);
			while(read(hijo[0], buffer_hijo, SIZE)>0)
				write(1, buffer_hijo, strlen(buffer_hijo));
				printf("%s", buffer_hijo);
				close(hijo[0]);
			}
		}
				



	return 0;
}

