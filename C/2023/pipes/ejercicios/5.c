#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>


#define LECTURA		0
#define ESCRITURA	1
int main (int argc, char *argv[]){
	
	int tuberia1[2], tuberia2[2];
	pid_t pid;
	
	pipe(tuberia1);
	pid = fork();

	if(pid == 0){
		// 0 lectura y 1 escrutura STDIN_FILENO y STDOUT_FILENO
		close(tuberia1[LECTURA]);
		dup2(tuberia1[ESCRITURA], ESCRITURA);
		close(tuberia1[ESCRITURA]);

		execlp("/bin/ls", "ls", "-l", NULL);
	}else{
		pipe(tuberia2);
		pid=fork();
		if(pid==0){
			// proceso 2
			close(tuberia1[ESCRITURA]);
			dup2(tuberia1[LECTURA], LECTURA);
			close(tuberia1[LECTURA]);
			
			close(tuberia2[LECTURA]);
			dup2(tuberia2[ESCRITURA], ESCRITURA);
			close(tuberia2[ESCRITURA]);

			execlp("/bin/grep", "grep", "u", NULL);
		}else{
			pid=fork();
			if(pid == 0){
				// proceso 3
				close(tuberia2[ESCRITURA]);
				dup2(tuberia2[LECTURA], LECTURA);
				close(tuberia2[LECTURA]);

				execlp("/bin/wc", "wc", "-l", NULL);

			}else{
				// padre
				wait(NULL);
			}
		}
	}

	return 0;
}
