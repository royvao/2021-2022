#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2){
	return op1 + op2;
}

int resta (int op1, int op2){
	return op1 - op2;
}

int preguntar_op (){
	static int nop = 0;
	int op;
	
/*	
 	int a = nop++;  POSTFIJO --> USA EL VALOR DE NOP Y LUEGO LO INCREMENTAS 
	int b = ++nop;	PREFIJO  --> PRIMERO INCREMENTA Y LUEGO USA EL VALOR
*/
	printf ("Operando %i: ", ++nop);
	scanf (" %i",&op);

	return op;	
}


int main () {
	
	int op1, op2,result;
	
	printf("BIENVENIDO AL PROGRAMA SUMA\n");	
	op1 = preguntar_op ();
	op2 = preguntar_op ();

	result= suma (op1,op2);
	
	printf("El resultado de sumar %i y %i da como resultado %i\n",op1,op2,result);	


	return EXIT_SUCCESS;

}
