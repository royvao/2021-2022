#include <stdio.h>
#include <stdlib.h>


int main ( ){
	char caracter = 'B';
	
	printf ("Caracter: %c\n", caracter);
	printf ("Caracter: %c\n", 'B');
	printf ("Caracter: %c\n", 0x42);

	printf("\n");

	printf ("Cadena: 1\n");
	printf ("Cadena: 1 Cadena 2\n");
	printf ("Cadena: 1"
		" "	  "Cadena 2\n");

	printf("\n");

	printf(
	"Usage:	letters [options]\n"
	"Prints letters.\n"
	);
	
	printf("\n");

	printf ("\
		Usage:  letters [options]\n\
		Prints Characters.	 \n\
					 \n\
		Options:		 \n\
	");


	printf("\n");

	printf("Nombre: %s\n", "Roy");

	const char * const name = "Roy"; //const califica a lo que queda a la izquierda
	printf("Nombre: %s\n", name);		

	return EXIT_SUCCESS;
}

