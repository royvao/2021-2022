#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2){
	return op1 + op2;
}

int resta (int op1, int op2){
	return op1 - op2;
}

int preguntar_op (){
	static int nop = 0;
	int op;
	
/*	
 	int a = nop++;  POSTFIJO --> USA EL VALOR DE NOP Y LUEGO LO INCREMENTAS 
	int b = ++nop;	PREFIJO  --> PRIMERO INCREMENTA Y LUEGO USA EL VALOR
*/
	printf ("Operando %i: ", ++nop);
	scanf (" %i",&op);

	return op;	
}

void gotoxy(int x, int y){

printf("%c[%d;%df",0x1B,y,x);

}


int main () {
	
	int op1, op2,result;
	int x=50, y=20;
	system("clear");
	printf("BIENVENIDO AL PROGRAMA SUMA\n");	
	op1 = preguntar_op ();
	op2 = preguntar_op ();

	result= suma (op1,op2);

	gotoxy(x,y);

	printf("\t\x1b[5;47;30m%0*i\x1b[0m\n",6,result);	

	return EXIT_SUCCESS;

}
