#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define MAX 100
#define FLAKE "❄" //2744

void copo (unsigned i, int x, const char* c){

	printf ("\033[%u;%iH%s\n", i , x, c);
}

int main(){

	int x;

	srand (time(NULL));

	for (unsigned i=0; i<MAX; i++){

		x = (rand () % 18)-8;
	
		printf ("\033[2J");

		for (int a=0; a<MAX; a++){
		copo (i, 20+x, FLAKE);   
		x = (rand () % 250)-20;
		i = (rand () % 100);
		}

		usleep(500000);

	}

	return 0;
}


