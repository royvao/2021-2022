#include <stdio.h>
#include <strings.h>

int main (){

	char buffer[0x20];

	printf("Nombre: ");
	scanf(" %s", buffer);

	if (strcasecmp (buffer, "roy") == 0) {
		fprintf (stderr, "Tú no puedes jugar.\n");
		return 1;
	}

	printf ("Hola, %s\n", buffer);

	return 0;
}
