#include <stdio.h>
#include <stdlib.h>

#define MAX 20
int function() {
	//Definimos el array con un máximo de 10
	char mander[MAX];

	//Creamos el bucle que vaya sumando caracteres de 0 a 9, y lo metemos en el array
	for (char i=0; i<10; i++){
		//Multiplicamos por 2 el valor de i para que imprima después el doble de cada número
		mander[i]=i*2; 
		printf ("%i\n", mander[i]);
	}
}
int main (){
	function();
}
