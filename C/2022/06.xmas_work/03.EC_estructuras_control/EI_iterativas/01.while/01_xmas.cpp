#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <string.h>

#define MAX 30

int main () {
	char phrase[MAX];
	int number = 0;

	printf ("Introduce una frase: ");
	fgets (phrase, MAX, stdin);
	char strlenPhrase = strlen(phrase);

	while (number < strlenPhrase){
		printf ("%c", phrase[number]);
		number++;
	}
	return EXIT_SUCCESS;
}

