#include <stdio.h>
#include <stdlib.h>
#include <cstring>

int main () {
	int number;

	do {
		printf ("Introduce un número entre 1 y 10: ");
		scanf ("%i", &number);
	}
	while (number < 1 || number >10);
		printf ("Su número es el %i\n", number);
		
	return EXIT_SUCCESS;
}

