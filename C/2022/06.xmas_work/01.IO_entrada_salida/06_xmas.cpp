#include <stdio.h>
#include <stdlib.h>

int main () {
	int number1;
	int number2;
	int resultado;
	
	printf ("Escribe operando número 1: ");
	scanf (" %i", &number1);
	printf ("Escribe operando número 2: ");
	scanf (" %i", &number2);

	resultado = number1 + number2;
	printf ("\x1b[34m%i + \x1b[34m%i = \x1b[32m%i \n", number1, number2, resultado);
		
	return EXIT_SUCCESS;
}

