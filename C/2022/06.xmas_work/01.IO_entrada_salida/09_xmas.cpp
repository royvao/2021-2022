#include <stdio.h>
#include <stdlib.h>

int main () {
	int number;

	printf ("Escribe un número en decimal: ");
	scanf (" %i", &number);
	printf ("El número introducido convertido a hexadecimal es: %X\n", number);

	return EXIT_SUCCESS;
}

