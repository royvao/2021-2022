#include <stdio.h>
#include <stdlib.h>

int main () { 
	int number;

	printf ("Escribe un número: ");
       	scanf (" %i", &number);

	if (number > 32 && number < 128) { 
		printf ("El número introducido es: %i y %c\n", number, number); 
	}
       	else{ 
		printf ("Ese número no se encuentra entre 32 y 128\n");
	}

	return EXIT_SUCCESS;
}

