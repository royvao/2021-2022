#include <stdio.h>
#include <stdlib.h>

#define MAX 10
int main () {
	int array[MAX];
	int elementSize, dateType, elementQuantity;

	elementSize = sizeof(array);
	dateType = sizeof(int);

	elementQuantity = elementSize / dateType;

	printf ("La cantidad de elementos de un array es de %i\n", elementQuantity);

	return EXIT_SUCCESS;
}

