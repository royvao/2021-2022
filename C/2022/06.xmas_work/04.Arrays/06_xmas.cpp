#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {
	int array[3][10];
	int i;
	int a;
		
	for (i=0; i<3; i++){
		for (a=0; a<10; a++){
			array[0][a]=a;
			array[1][a]= pow(a, 2);
			array[2][a]= pow(a, 3);
		}
	}
	
	for (i=0; i<3; i++){
		for (a=0; a<10; a++){
			printf("%i", array[i][a]);
		}
		printf ("\n");
	}
	return EXIT_SUCCESS;
}

