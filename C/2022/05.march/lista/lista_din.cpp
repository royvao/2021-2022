#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define		MAXPAL 		100

int main (){
	
	/* DECLARAR VARIABLES */
	unsigned len;
	unsigned npal = 0;
	char buff[MAXPAL];
	char **lista = NULL;
	
	/* INICIALIZACIÓN */ 
	
	/* ENTRADA DE DATOS */
	while (strcasecmp (buff, "fin") != 0){
		system ("clear");
		printf ("Palabra: ");
		scanf (" %[^\n]", buff);
		__fpurge (stdin);

		len = strlen(buff) + 1;
		lista = (char **) realloc (lista, (npal + 1) * sizeof(char *));
		lista[npal] = (char *) malloc (len);
		strcpy (lista[npal], buff);
		npal++;
	}
	
	/* CALCULOS */
	
	/* SALIDA DE DATOS*/
	printf ("\n");
	for (unsigned i=0; i<npal; i++)
		printf ("%i.- %s\n", i+1, lista[i]);
	printf ("\n");

	/* LIBERACION */
	for (unsigned i=0; i<npal; i++)
		free (lista[i]);
	free (lista);

	return EXIT_SUCCESS;
}
