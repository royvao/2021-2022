#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define 	N 	4
#define 	UP	"\x1B[2A\n"

enum TColor { vacio, rojo, azul, amarillo, verde};

const int ansi_color[] = {101,104,103,102};

void iniciar_juego(){	
	system ("clear");
	system ("toilet -fpagga --gay 'MASTERMIND'");
	printf ("\nColores:\n1- Rojo\n2- Azul\n3- Amarillo\n4- Verde\n");
	printf ("\n");
}

void show (char secret[N]) {
	for (int i=0; i<N; i++){
		printf ("\x1B[%im ", ansi_color[secret[i]]);
		printf ("\x1B[30m%i ", secret[i]+1);
	}
	printf ("\n");
}

void leer_entrada (char secret[N]) {
	char input;
	for (int i=0; i<N; i++) {
		do {
			__fpurge (stdin);
			printf ("                    \r");
			printf ("Elige el %iº color: ", i + 1);
			input = getchar () - '0';
			printf (UP);
		} while (input <1 || input > N);
		secret [i] = input - 1;
	}
	printf ("\n\n");

}
void initgame(){

}

int main (){
	char secret[N];
	iniciar_juego();
	leer_entrada (secret);
	show (secret);
	printf ("\x1B[0m\n");


	return EXIT_SUCCESS;
}
